package com.mamirov.runner;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

public class EmailSender {
    public static void sendEmail(String message, String title){
        try {
            if(Config.emailReportingIsEnabled()){
                Email email = new SimpleEmail();
                email.setHostName(EmailConfig.getSmtpHost());
                email.setSmtpPort(EmailConfig.getSmtpPort());
                email.setAuthenticator(new DefaultAuthenticator(EmailConfig.getUser(), EmailConfig.getPassword()));
                email.setSSLOnConnect(false);
                email.setFrom(EmailConfig.getEmailFrom());
                email.setSubject(title);
                email.setMsg(message);
                email.addTo(EmailConfig.getEmailAddress());
                email.send();
            }
        } catch (EmailException e) {
            e.printStackTrace();
        }
    }
}
