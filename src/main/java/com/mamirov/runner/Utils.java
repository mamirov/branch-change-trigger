package com.mamirov.runner;

public class Utils {

    public static String USER_DIR = System.getProperty("user.dir");
    public static String FILE_SEPARATOR = System.getProperty("file.separator");

    public static String getPropertyError(String property){
        return "Check property '" + property + "' in " + USER_DIR + FILE_SEPARATOR + "config.properties";
    }
}
