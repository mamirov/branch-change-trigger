package com.mamirov.runner;

import java.io.FileInputStream;
import java.util.Properties;

public class EmailConfig {
    public static Properties props = new Properties();
    static {
        try{
            FileInputStream fis = new FileInputStream("email.properties");
            props.load(fis);
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getSmtpHost(){
        return props.getProperty("smtp.host.name");
    }

    public static Integer getSmtpPort(){
        return Integer.parseInt(props.getProperty("smtp.port"));
    }

    public static String getUser(){
        return props.getProperty("user");
    }

    public static String getPassword(){
        return props.getProperty("password");
    }

    public static String getEmailFrom(){
        return props.getProperty("email.from");
    }

    public static String getEmailAddress(){
        return props.getProperty("send.email.to");
    }
}
