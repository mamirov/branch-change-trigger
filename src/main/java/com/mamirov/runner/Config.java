package com.mamirov.runner;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {
    public static Properties props = new Properties();
    static {
        try{
            FileInputStream fis = new FileInputStream("config.properties");
            props.load(fis);
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setProp(String key, String value){
        try {
            props.setProperty(key, value);
            props.store(new FileOutputStream("config.properties"), "Branch changed");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getGitRepository(){
        return props.getProperty("git.repository.path");
    }

    public static String getCurrentBranch(){
        return props.getProperty("current.branch");
    }

    public static String getTeamCityUserPassword(){
        return props.getProperty("teamcity.basic.user.password");
    }

    public static String getBuildName(){
        return props.getProperty("teamcity.build.name");
    }

    public static String getGitBinPath(){
        return props.getProperty("git.bin.path");
    }

    public static String getTeamCityServerUrl(){
        return props.getProperty("teamcity.server.url");
    }

    public static Integer getUpdateTime(){
        return Integer.parseInt(props.getProperty("branch.change.updatetime.milliseconds"));
    }

    public static Integer getTeamCityUpdateTime(){
        return Integer.parseInt(props.getProperty("teamcity.buildtypes.updatetime.minutes"));
    }

    public static Boolean emailReportingIsEnabled(){
        return Boolean.parseBoolean(props.getProperty("enable.email.report"));
    }

    public static String getTeamCityBuildTypes(){
        return props.getProperty("teamcity.build.types");
    }
}
