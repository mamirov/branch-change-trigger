package com.mamirov.runner;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class TeamCityBuildRunner {

    public static HttpResponse getRunResponse(){
        try {
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet("http://"+Config.getTeamCityUserPassword()+"@"+Config.getTeamCityServerUrl()+"/httpAuth/action.html?add2Queue="+Config.getBuildName());
            return httpClient.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
