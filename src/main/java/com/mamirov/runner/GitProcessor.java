package com.mamirov.runner;

import java.io.*;
import java.util.Timer;
import java.util.TimerTask;

import static com.mamirov.runner.TeamCityBuildRunner.*;

public class GitProcessor {

    public static String getBranchName() {
        try {
            ProcessBuilder processBuilder = new ProcessBuilder();
            File file = new File(Config.getGitRepository());
            processBuilder.directory(file);
            Process process = processBuilder.command(Config.getGitBinPath(), "symbolic-ref", "--short", "HEAD").start();
            String gitStatus = readProcessStream(process.getInputStream());
            if(gitStatus.isEmpty()){
                String commandError = readProcessStream(process.getErrorStream())+"\n"+Utils.getPropertyError("git.repository.path");
                System.out.println(commandError);
                EmailSender.sendEmail(commandError, "Invalid path to Git repository");
                System.exit(0);
            }
            else {
                return gitStatus.replace("\n", "");
            }
        } catch (Exception e){
            e.printStackTrace();
            String gitBinPathError = e.getLocalizedMessage() + "\n"+Utils.getPropertyError("git.bin.path");
            System.out.println(gitBinPathError);
            EmailSender.sendEmail(gitBinPathError, "Invalid path to Git bin");
            System.exit(0);
        }
        return "";
    }

    public static String readProcessStream(InputStream inputStream){
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
            StringBuilder stringBuilder = new StringBuilder();
            Reader reader = new BufferedReader(inputStreamReader);
            int ch;
            while ((ch = reader.read())>-1){
                stringBuilder.append((char) ch);
            }
            reader.close();
            return stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getBuildTypes(){
        StringBuilder sb = new StringBuilder();
        String [] splitBuildTypes = Config.getTeamCityBuildTypes().split(",");
        for(String buildType : splitBuildTypes){
            sb.append(buildType+"\n");
        }
        return sb.toString();
    }

    public static void validateProperties(){
        if(Config.getTeamCityBuildTypes().contains(Config.getBuildName())){
        }
        else {
            String buildNameError = "Build name: '"+Config.getBuildName()+"' not find in TeamCity build types\n"+Utils.getPropertyError("teamcity.build.name")+"\n\nActual build types:\n"+getBuildTypes();
            System.out.println(buildNameError);
            EmailSender.sendEmail(buildNameError, "Build name not find");
            System.exit(0);
        }
    }

    public static void detectBranchChanges(){
        String branchName = getBranchName();
        String message = "Branch '"+Config.getCurrentBranch()+"' changed to '"+branchName+"'.\nRunning build "+Config.getBuildName()+"..\n WebUrl: http://"+Config.getTeamCityServerUrl()+"/viewType.html?buildTypeId="+Config.getBuildName();
        String authError = "401 Error. Wrong TeamCity user or password.\n"+Utils.getPropertyError("teamcity.basic.user.password");
        if(Config.getCurrentBranch().equals(branchName)){
            System.out.println("No changes detected");
        }
        else {
            int responseStatus = getRunResponse().getStatusLine().getStatusCode();
            if(responseStatus == 401){
                System.out.println(authError);
                EmailSender.sendEmail(authError, "401 Error");
                System.exit(0);
            }
            else if(responseStatus == 200){
                System.out.println(message);
                EmailSender.sendEmail(message, "Branch changed to "+branchName);
                Config.setProp("current.branch", branchName);
            }
            else {
                System.out.println(responseStatus+" Error");
                EmailSender.sendEmail(responseStatus + " Error", responseStatus + " Error");
                System.exit(0);
            }
        }
    }

    public static void main(String [] args){
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                detectBranchChanges();
            }
        };
        TimerTask updateBuildTypesTimerTask = new TimerTask() {
            @Override
            public void run() {
                TeamCityXMLParser.updateBuildTypes();
                validateProperties();
            }
        };
        timer.scheduleAtFixedRate(updateBuildTypesTimerTask, 0, (1000*60)*Config.getTeamCityUpdateTime());
        timer.scheduleAtFixedRate(timerTask, 0, Config.getUpdateTime());
    }
}
