package com.mamirov.runner;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import sun.misc.BASE64Encoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class TeamCityXMLParser {

    public static NodeList getNodeList(String xmlUrl, String tag) {
        try{
            URL url = new URL(xmlUrl);
            String userPassword = Config.getTeamCityUserPassword();
            String encoding = new BASE64Encoder().encode(userPassword.getBytes());
            URLConnection uc = url.openConnection();
            uc.setRequestProperty("Authorization","Basic " + encoding);
            uc.connect();
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(uc.getInputStream()));
            doc.getDocumentElement().normalize();
            return doc.getElementsByTagName(tag);
        }
        catch (Exception e){
            String error = "401 Error. Wrong user or password.\n"+Utils.getPropertyError("teamcity.basic.user.password");
            System.out.println(error);
            EmailSender.sendEmail(error, "401 Error");
            e.printStackTrace();
        }
        return null;
    }

    public static List<String> getProjectHref(){
        List<String> projectHrefs = new ArrayList<String>();
        int nodeSize = getNodeList("http://"+Config.getTeamCityServerUrl()+"/httpAuth/app/rest/projects", "project").getLength();
        for(int i = 0; nodeSize>i; i++){
            String project = getNodeList("http://"+Config.getTeamCityServerUrl()+"/httpAuth/app/rest/projects", "project").item(i).getAttributes().getNamedItem("href").getNodeValue();
            if(project.contains("oot")){
            }
            else {
                projectHrefs.add(project);
            }
        }
        return projectHrefs;
    }

    public static void updateBuildTypes(){
        List<String> buildTypeIds = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        for(String projectHref : getProjectHref()){
            int nodeSize = getNodeList("http://"+Config.getTeamCityServerUrl()+projectHref, "buildType").getLength();
            for(int i=0; nodeSize>i; i++){
                buildTypeIds.add(getNodeList("http://"+Config.getTeamCityServerUrl()+projectHref, "buildType").item(i).getAttributes().getNamedItem("id").getNodeValue());
            }
        }
        for(String buildType : buildTypeIds){
            if(buildTypeIds.get(buildTypeIds.size()-1).equals(buildType)){
                sb.append(buildType);
            }
            else {
                sb.append(buildType+",");
            }
        }
        Config.setProp("teamcity.build.types", sb.toString());
        System.out.println("TeamCity Build Types Updated");
    }
}
