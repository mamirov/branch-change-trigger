﻿### Branch change trigger

Приложение, которое отслеживает переключение веток и запускат билд в TeamCity.

## Требования:
- Java 1.7
- Maven
- TeamCity 8.x

## Конфигурация:
# config.properties:
*git.repository.path*

Здесь указывается абсолютный путь до отслеживаего репозитория Git.

*teamcity.build.name*

Указываем название билда, которое будет запускаться при переключении ветки

*current.branch*

Параметр по умолчанию, в него будет автоматически записываться текущая ветка

*teamcity.build.types*

Параметр по умолчанию, в него будут автоматически записываться все билд конфиги с тимсити

*teamcity.server.url*

Ваш адрес тимсити сервера, например: teamcity.mycompany.com

*git.bin.path*

Указываем абсолютный путь до бинарника Git.

*branch.change.updatetime.milliseconds*

Указываем через какое время будет обновлять данные переключения веток в миллисекундах

*teamcity.buildtypes.updatetime.minutes*

Указываем частоту обновления получения билд конфигов с тимсити, в минутах.

*teamcity.basic.user.password*

Указываем данные basic авторизации в тимсити, например: user:password

*enable.email.report*

Включение репортинга по почте, true/false